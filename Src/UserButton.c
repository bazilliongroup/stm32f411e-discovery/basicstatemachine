/*
Library:			UserButton.c
Written by:			Ronald Bazillion
Date written:			07/03/2019
Description:			User Button functions
Resources:                      
            
Modifications:
-----------------

*/

//***** Header files *****//
#include "UserButton.h"
#include "TimerModule.h"
#include "BasicStateMachine.h"
#include "main.h"

extern FUNCT_PTR SM_StateMachine;

//***** Defines ********//

//***** typedefs enums *****//

//***** typedefs structs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//
static bool m_usrButtonPressed;
static volatile bool m_usrButtonDebounce;
static volatile uint8_t m_tmrBtnDebounce;

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

void UserButtonInit(void)
{
    m_usrButtonPressed = false;
    m_usrButtonDebounce = false;
    
    m_tmrBtnDebounce  = TmrCreate();
    if (m_tmrBtnDebounce != UNSUCCESSFUL)
    {
      if (TmrPopulate(m_tmrBtnDebounce, 500, UserButtonDbnCheck, NULL, true))
      {
          TmrReset(m_tmrBtnDebounce);
      }
      else
      {
        TmrDelete(m_tmrBtnDebounce);
      }
    }
}

void UserButtonCheck(void)
{
    if ((m_usrButtonPressed == false) &&  //previous button state is not pressed
        (m_usrButtonDebounce == false) && //debounce not set
        (HAL_GPIO_ReadPin(UserButton_GPIO_Port, UserButton_Pin) == GPIO_PIN_SET)) //current button is being pressed
      {
            m_usrButtonPressed = true;    //set button as being presssed
            TmrReset(m_tmrBtnDebounce);   //reset debounce
            TmrStart(m_tmrBtnDebounce);   //start debounce
      }
}

void UserButtonDbnCheck(uint8_t var)
{
  if (HAL_GPIO_ReadPin(UserButton_GPIO_Port, UserButton_Pin) == GPIO_PIN_SET)
  {
    m_usrButtonDebounce = true;           //debounce is true
  }
  else
  {
    m_usrButtonDebounce = false;
    m_usrButtonPressed = false;  
  }
}

void UserButtonSetPressed(bool val)
{
    m_usrButtonPressed = val;
}
          
bool UserButtonGetPressed(void)
{
  bool ret = false;
  if (m_usrButtonPressed == true)
  {
    ret = true;
  }
  return ret;
}

void UserButtonSetDbn(bool val)
{
    m_usrButtonDebounce = val;
}

bool UserButtonGetDbn(void)
{
  bool ret = false;
  if (m_usrButtonDebounce == true)
  {
    ret = true;
  }
  return ret;
}          

//***********************************************************************************
//***************************** STATIC FUNCTIONS ************************************
//***********************************************************************************