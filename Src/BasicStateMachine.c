/*
Library:			BasicStateMachine.c
Written by:			Ronald Bazillion
Date written:			07/02/2019
Description:			Implementation File for Basic State Machine
Resources:                      https://www.youtube.com/watch?v=pxaIyNbcPrA&index=9&list=PL5701E4325336503E&t=53s
                                https://www.youtube.com/playlist?list=PLn8PRpmsu08pHsOznk_WT6QT9cySw1A59
                                https://help.ubidots.com/en/articles/1161418-mqtt-finite-state-machines-and-ubidots-part-i
                                https://ubidots.com/blog/advantages_and_disadvantages_of_finite_state_machines/amp/?fbclid=IwAR13Hoih8N9JjepKhloYYnhRqDKZTXiNmeo3pCk-oGfVgSVG3g3hrMFKDdo
            
Modifications:
-----------------

*/

//***** Header files *****//
#include "BasicStateMachine.h"
#include "TimerModule.h"
#include "UserButton.h"
#include "main.h"

//***** Defines ********//
//#define DEBUG

//***** typedefs enums *****//


//***** typedefs structs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//

static void m_actionState1Event1(void);
static void m_actionState1Event2(void);
static void m_actionState2Event1(void);
static void m_actionState2Event2(void);
static void m_actionState3Event1(void);
static void m_actionState3Event2(void);
static void m_actionState4Event1(void);
static void m_actionState4Event2(void);

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

STATE_MACHINE_STRUCT SM_StateMachine;   //global variable used for State Machine; must be extern to use.

//table that processes events. the table is an array of function pointers.
FUNCT_PTR eventFctPointerTable[MAX_STATES][MAX_EVENTS] =
{
  { m_actionState1Event1, m_actionState1Event2 }, /* procedures for state1 */
  { m_actionState2Event1, m_actionState2Event2 }, /* procedures for state2 */
  { m_actionState3Event1, m_actionState3Event2 }, /* procedures for state3 */
  { m_actionState4Event1, m_actionState4Event2 }  /* procedures for state4 */
};

void SM_Init(void)
{
  UserButtonInit();
  HAL_GPIO_WritePin(RedLED_GPIO_Port, RedLED_Pin, GPIO_PIN_SET);
  SM_StateMachine.States = RED_STATE;
  SM_StateMachine.FuncPtr = SM_RedState;
}

void SM_CommonTasks(void)
{
  UserButtonCheck();  
}

void SM_RedState(void)
{ 
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable[RED_STATE][EVENT1]();
    }
}

void SM_BlueState(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable[BLUE_STATE][EVENT1]();
    }  
}

void SM_GreenState(void)
{  
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable[GREEN_STATE][EVENT1]();
    }
}

void SM_OrangeState(void)
{  
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable[ORANGE_STATE][EVENT1]();
    }  
}

//***********************************************************************************
//***************************** STATIC FUNCTIONS ************************************
//***********************************************************************************

static void m_actionState1Event1(void)  //red state event1 --> blue state
{
    HAL_GPIO_WritePin(RedLED_GPIO_Port, RedLED_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(BlueLED_GPIO_Port, BlueLED_Pin, GPIO_PIN_SET);
    SM_StateMachine.States = BLUE_STATE;
    SM_StateMachine.FuncPtr = SM_BlueState;
}

static void m_actionState1Event2(void)  //red state event2 - no transitions for now
{
    SM_StateMachine.States = RED_STATE;
    SM_StateMachine.FuncPtr = SM_RedState;
}

static void m_actionState2Event1(void)  //blue state event1 --> green state
{
    HAL_GPIO_WritePin(BlueLED_GPIO_Port, BlueLED_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GreenLED_GPIO_Port, GreenLED_Pin, GPIO_PIN_SET);
    SM_StateMachine.States = GREEN_STATE;
    SM_StateMachine.FuncPtr = SM_GreenState;
}

static void m_actionState2Event2(void)  //blue state event2 - no transitions for now
{
    SM_StateMachine.States = BLUE_STATE;
    SM_StateMachine.FuncPtr = SM_BlueState;
}

static void m_actionState3Event1(void)  //green state event1 --> orange state
{
    HAL_GPIO_WritePin(GreenLED_GPIO_Port, GreenLED_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(OrangeLED_GPIO_Port, OrangeLED_Pin, GPIO_PIN_SET);
    SM_StateMachine.States = ORANGE_STATE;
    SM_StateMachine.FuncPtr = SM_OrangeState;    
}

static void m_actionState3Event2(void)  //green state event2 - no transitions for now
{
    SM_StateMachine.States = GREEN_STATE;
    SM_StateMachine.FuncPtr = SM_GreenState;  
}

static void m_actionState4Event1(void) //orange state event1 --> red state
{
    HAL_GPIO_WritePin(OrangeLED_GPIO_Port, OrangeLED_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(RedLED_GPIO_Port, RedLED_Pin, GPIO_PIN_SET);
    SM_StateMachine.States = RED_STATE;
    SM_StateMachine.FuncPtr = SM_RedState;  
}

static void m_actionState4Event2(void)  //orange state event2 - no transitions for now
{
    SM_StateMachine.States = ORANGE_STATE;
    SM_StateMachine.FuncPtr = SM_OrangeState;  
}