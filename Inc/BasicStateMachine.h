/*
Library:			BasicStateMachine.h
Written by:			Ronald Bazillion
Date written:			07/02/2019
Description:			Header File for Basic State Machine
Resources:                      https://www.youtube.com/watch?v=pxaIyNbcPrA&index=9&list=PL5701E4325336503E&t=53s
                                https://www.youtube.com/playlist?list=PLn8PRpmsu08pHsOznk_WT6QT9cySw1A59
            
Modifications:
-----------------

*/

#ifndef __BASIC_STATE_MACHINE_H_
#define __BASIC_STATE_MACHINE_H_

//*******Include*****************//
#include "stm32f4xx_hal.h"

//*******Defines*****************//
#define MAX_STATES 4
#define MAX_EVENTS 2

//*******Enums*****************//

typedef enum
{
  RED_STATE,
  BLUE_STATE, 
  GREEN_STATE,
  ORANGE_STATE,
}BASIC_STATES;

typedef enum
{
  EVENT1,
  EVENT2, 
}BASIC_EVENTS;

//*******Flags****************//

//*****Structures *************//

typedef void (*FUNCT_PTR)(void);

typedef struct
{
  BASIC_STATES States;
  BASIC_EVENTS Events;
  FUNCT_PTR FuncPtr;            // My function Pointer to the state functions.        
}STATE_MACHINE_STRUCT;



//***** Constant Variables *****//

//***** API Functions prototype *****//

/* Name: SM_Init(void) 
   Description: Used to initialize the State Machine
   Inputs args: None
   Output args: None 
*/
void SM_Init(void);

/* Name: SM_CommonTasks(void) 
   Description: entry POint for common functions that will be called regardless of any state.
   Inputs args: None
   Output args: None 
*/
void SM_CommonTasks(void);

//state machine functions
void SM_RedState(void);
void SM_BlueState(void);
void SM_GreenState(void);
void SM_OrangeState(void);

#endif