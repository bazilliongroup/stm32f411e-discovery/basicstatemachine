/*
Library:			UserButton.h
Written by:			Ronald Bazillion
Date written:			07/03/2019
Description:			User Button functions
Resources:                      
            
Modifications:
-----------------

*/

#ifndef __USER_BUTTON_H_
#define __USER_BUTTON_H_

//***** Header files *****//
#include "stm32f4xx_hal.h"
#include <stdbool.h>

//***** Defines ********//

//*******Enums*****************//

//*******Flags****************//

//*****Structures *************//

//***** Constant Variables *****//

//***** API Functions prototype *****//

/* Name: UserButtonInit(void) 
   Description: Initialize the user Button
   Inputs args: None
   Output args: None 
*/
void UserButtonInit(void);

/* Name: UserButtonCheck(void) 
   Description: Function used to check the user button. If button pressed it will initilaize the debounce timer.
   Inputs args: None
   Output args: None 
*/
void UserButtonCheck(void);

/* Name: UserButtonDbnCheck(uint8_t var) 
   Description: debounce timer interrupt function.
   Inputs args: var - necessary for the interrupt function format allows you to add in an 8 bit value.
   Output args: None 
*/
void UserButtonDbnCheck(uint8_t var);

/* Name: UserButtonSetPressed(bool val) 
   Description: sets the User Button Pressed value
   Inputs args: var - boolean value used to set variable.
   Output args: None 
*/
void UserButtonSetPressed(bool val);

/* Name: UserButtonGetPressed(void) 
   Description: gets the User Button Pressed value
   Inputs args: None
   Output args: true - user button pressed, false - user button not pressed
*/
bool UserButtonGetPressed(void);

/* Name: UserButtonSetPressed(bool val) 
   Description: sets the User Button Debounced value
   Inputs args: var - boolean value used to set variable.
   Output args: None 
*/
void UserButtonSetDbn(bool val);

/* Name: UserButtonGetDbn(void) 
   Description: gets the User Button Debounced value
   Inputs args: None
   Output args: true - user button pressed, false - user button not pressed
*/
bool UserButtonGetDbn(void);

#endif